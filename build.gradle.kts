buildscript {
    repositories {
        mavenLocal()
    }
    dependencies {
        fun embeddedKotlin(name: String) =
            "org.jetbrains.kotlin:kotlin-$name:1.6.10"
        classpath(embeddedKotlin("gradle-plugin"))
        classpath(embeddedKotlin("serialization"))

        val shadow = "com.github.jengelman.gradle.plugins:shadow:6.1.0"
        classpath(shadow)
    }
}

plugins {
    kotlin("jvm") version "1.6.10" //Versi Kotlin terakhir
    kotlin("kapt") version "1.6.10"
    id("com.github.johnrengelman.shadow") version "5.0.0"
}

group = "com.fzulram"
version = "0.1-SNAPSHOT"

allprojects { repositories { mavenCentral() } }