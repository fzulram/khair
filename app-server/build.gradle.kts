import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    application
    kotlin("jvm")
    kotlin("plugin.serialization")
    id("com.github.johnrengelman.shadow")
}

application {
    mainClassName =
        "io.ktor.server.netty.EngineMain"
}

group = "com.fzulram"
version = "1.0-SNAPSHOT"

/**
 * jvmTarget sesuaikan dengan
 * JDK yang digunakan.
 *
 * Kalau pakai OpenJDK untuk
 * production (engga sekedar latihan),
 * pastikan pakai versi yang dapet
 * Long Terms Support (LTS).
 * Contoh : 1.8, 11, 17.
 */
tasks.withType<KotlinCompile>().all {
    kotlinOptions {
        jvmTarget = "11"
    }
}

tasks.shadowJar {
    archiveBaseName.set("Khair-Server")
    manifest {
        val mc = application.mainClassName
        val pn = project.name
        val av = archiveVersion.get()

        attributes(
            mapOf(
                "Main-Class"
                    to mc,
                "Implementation-Title"
                    to pn,
                "Implementation-Version"
                    to av
            )
        )
    }
}

val serialization_version: String by project
val log4j_version: String by project
val ktor_version: String by project
fun log4J(name: String) =
    "org.apache.logging.log4j:$name:$log4j_version"
fun ktor(name: String) =
    "io.ktor:ktor-$name:$ktor_version"
fun serialization(name: String) =
    "org.jetbrains.kotlinx:" +
        "kotlinx-serialization-$name:" +
        serialization_version

dependencies {
    implementation(kotlin("stdlib-jdk8"))

    implementation(project(":domain"))

    //Ktor
    implementation(
        ktor("server-netty")
    )
    implementation(
        ktor("server-core")
    )
    implementation(
        ktor("server-host-common")
    )
    implementation(
        ktor("websockets")
    )
    implementation(
        ktor("auth")
    )
    implementation(
        ktor("auth-jwt")
    )
    implementation(
        ktor("serialization")
    )

    implementation(log4J("log4j-slf4j-impl"))

    implementation(serialization("json-jvm"))
    testImplementation(kotlin("test"))
}