package com.fzulram.khair

import com.fzulram.khair.country.installCountryRoute
import com.fzulram.khair.jwt.Jwt
import com.fzulram.khair.auth.installAuthRoute
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.auth.jwt.*
import io.ktor.features.*
import io.ktor.http.content.*
import io.ktor.routing.*
import io.ktor.serialization.*
import kotlinx.serialization.json.Json

fun Application.module() {
    install(Authentication) {
        jwt {
            realm = Jwt.realm
            verifier(Jwt.verifier)

            validate { credential ->
                Jwt.verify(credential)
            }
        }
    }

    install(ContentNegotiation) {
        val mJson = Json {
            ignoreUnknownKeys = true
            prettyPrint = true
        }

        json(mJson)
    }

    routing {
        static("/") {
            files("static")
        }

        installCountryRoute()
        installAuthRoute()
    }
}