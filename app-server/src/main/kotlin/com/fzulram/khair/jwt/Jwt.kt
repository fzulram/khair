package com.fzulram.khair.jwt

import com.auth0.jwt.JWT
import com.auth0.jwt.JWTVerifier
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.exceptions.JWTVerificationException
import com.auth0.jwt.interfaces.DecodedJWT
import com.fzulram.khair.user.User
import com.fzulram.khair.user.UserRefreshPassword
import io.ktor.auth.*
import io.ktor.auth.jwt.*
import java.io.FileReader
import java.util.*
import kotlin.random.nextInt

object Jwt {

    /**
     * [SECRET_KEY] bersifat sangat rahasia,
     * pastikan kamu jaga dan simpan di tempat
     * yang paling aman. Jangan beritahukan kepada siapapun,
     * kecuali partner yang memang benar-benar kamu percaya.
     */
    private const val SECRET_KEY = "THIS IS A VERY SECRET KEY"

    private val ALGORITHM = Algorithm.HMAC512(SECRET_KEY)
    private const val SUBJECT = "Authentication"
    private const val USER_NAME = "user's name"
    private const val PASSWORD = "password"
    private val issuer: String
    private val audience: String
    private val validityInMillis: Long
    val realm: String
    val verifier: JWTVerifier

    init {
        val properties = Properties().apply {
            val reader = FileReader("jwt.properties")
            load(reader)
        }

        with(properties) {
            issuer = getProperty("domain")
            audience = getProperty("audience")
            realm = getProperty("realm")
            validityInMillis =
                getProperty("validity_in_millis").toLong()
        }
        verifier = JWT.require(ALGORITHM).apply {
            withIssuer(issuer)
            withAudience(audience)
        }.build()
    }

    fun verify(credential: JWTCredential): Principal? {
        return if (audience in credential.audience) {
            val name = credential[USER_NAME] ?: return null
            UserRefreshPassword[name] ?: return null

            UserIdPrincipal(name)
        } else null
    }

    fun createToken(user: User): String = JWT.create().apply {
        withSubject(SUBJECT)
        withIssuer(issuer)
        withAudience(audience)

        val expTime =
            Date(System.currentTimeMillis() + validityInMillis)
        withExpiresAt(expTime)

        withClaim(USER_NAME, user.name)
    }.sign(ALGORITHM)

    fun createToken(decodedJWT: DecodedJWT): String = JWT.create().apply {
        withSubject(SUBJECT)
        withIssuer(issuer)
        withAudience(audience)

        val expTime =
            Date(System.currentTimeMillis() + validityInMillis)
        withExpiresAt(expTime)
        val name = decodedJWT.getClaim(USER_NAME).asString()

        withClaim(USER_NAME, name)
    }.sign(ALGORITHM)

    fun createRefreshToken(
        user: User
    ): String = JWT.create().apply {
        withSubject(SUBJECT)
        withIssuer(issuer)
        withAudience(audience)

        val password = createRefreshPassword(user)
        withClaim(USER_NAME, user.name)
        withClaim(PASSWORD, password)
        UserRefreshPassword[user.name] = password
    }.sign(ALGORITHM)

    fun verifyRefreshToken(
        refreshToken: String,
        oldToken: String
    ): DecodedJWT? {
        val refreshDecoded = verify(refreshToken) ?: return null
        val accessDecoded = verify(oldToken) ?: return null

        val name1 = refreshDecoded.getClaim(USER_NAME).asString()
        val name2 = accessDecoded.getClaim(USER_NAME).asString()

        return if (name1 == name2) {
            val password = refreshDecoded.getClaim(PASSWORD).asString()
            if (password == UserRefreshPassword[name1]) {
                refreshDecoded
            } else null
        } else null
    }

    private fun verify(token: String): DecodedJWT? {
        val decoded = try {
            verifier.verify(token)
        } catch (ex: JWTVerificationException) {
            return null
        }

        if (audience !in decoded.audience) {
            return null
        }

        return decoded
    }

    private fun createRefreshPassword(
        user: User
    ): String {
        val builder = StringBuilder().apply {
            val random = kotlin.random.Random
            val times = random.nextInt(15, 20)
            repeat(times) {
                val name = user.name
                val index = random.nextInt(name.indices)
                append(name[index])
            }
        }

        return builder.toString()
    }
}