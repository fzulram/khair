package com.fzulram.khair.auth

import com.fzulram.khair.jwt.AuthToken
import com.fzulram.khair.jwt.Jwt
import com.fzulram.khair.user.USER
import com.fzulram.khair.user.User
import com.fzulram.khair.user.UserRefreshPassword
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*

/**
 * @author FzulRam.
 */
class AuthRoute {

    fun Route.login() = post("/login") {
        val user = call.receiveOrNull<User>()
        if (user == null) {
            val msg = "Cannot receive User from the request."
            call.respond(HttpStatusCode.BadRequest, msg)
            return@post
        }

        if (user == USER) {
            val accessToken = Jwt.createToken(user)
            val refreshToken = Jwt.createRefreshToken(user)
            val response = AuthToken(accessToken, refreshToken)
            call.respond(HttpStatusCode.OK, response)
        } else {
            val msg = "Name or Password is incorrect."
            call.respond(HttpStatusCode.Unauthorized, msg)
        }
    }

    fun Route.refreshAccessToken() = post("/refresh-access-token") {
        val authToken = call.receiveOrNull<AuthToken>()
        if (authToken == null) {
            val msg = "Cannot receive AuthToken from the request."
            call.respond(HttpStatusCode.BadRequest, msg)
            return@post
        }

        val decoded = with(authToken) {
            Jwt.verifyRefreshToken(refreshToken, accessToken)
        } ?: run {
            call.respond(HttpStatusCode.Unauthorized)
            return@post
        }

        val newToken = Jwt.createToken(decoded)
        val newAuthToken = authToken.copy(newToken)
        call.respond(HttpStatusCode.OK, newAuthToken)
    }

    fun Route.logout() = post("/logout") {
        val principal = call.principal<UserIdPrincipal>()!!
        UserRefreshPassword[principal.name] = null

        call.respond("Logout success.")
    }
}

fun Routing.installAuthRoute() {
    route("/auth") {
        with(AuthRoute()) {
            login()
            refreshAccessToken()
            authenticate {
                logout()
            }
        }
    }
}