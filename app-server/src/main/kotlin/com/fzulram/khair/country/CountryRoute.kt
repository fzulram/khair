package com.fzulram.khair.country

import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*

/**
 * @author Caramel Eat.
 */
class CountryRoute {

    fun Route.create() = post {
        val country = call.receiveOrNull<Country>()
        if (country == null) {
            val msg = "Cannot receive Country from the request."
            call.respond(HttpStatusCode.BadRequest, msg)
            return@post
        }

        val mName = country.name.lowercase()
        if (mName in countries.map { it.name.lowercase() }) {
            val msg = "Country already exists."
            call.respond(HttpStatusCode.Conflict, msg)
            return@post
        }

        countries.add(country)
        val msg = "Country successfully created."
        call.respond(HttpStatusCode.Created, msg)
    }

    fun Route.update() = put("/{name}") {
        val name = call.parameters["name"]
        if (name.isNullOrBlank()) {
            val msg = "Name cannot be null or blank."
            call.respond(HttpStatusCode.BadRequest, msg)
            return@put
        }

        val index = countries.indexOfFirst { it.name == name }
        if (index == -1) {
            val msg = "Country $name not found."
            call.respond(HttpStatusCode.NotFound, msg)
            return@put
        }

        val country = call.receiveOrNull<Country>()
        if (country == null) {
            val msg = "Cannot receive Country from the request."
            call.respond(HttpStatusCode.BadRequest, msg)
            return@put
        }

        countries[index] = country
        val msg = "Country successfully updated."
        call.respond(HttpStatusCode.OK, msg)
    }

    fun Route.deleteByName() = delete("/{name}") {
        val name = call.parameters["name"]
        if (name.isNullOrBlank()) {
            val msg = "Name cannot be null or blank."
            call.respond(HttpStatusCode.BadRequest, msg)
            return@delete
        }

        val index = countries.indexOfFirst { it.name == name }
        if (index == -1) {
            val msg = "Country $name not found."
            call.respond(HttpStatusCode.NotFound, msg)
            return@delete
        }

        countries.removeAt(index)
        val msg = "Country successfully deleted."
        call.respond(HttpStatusCode.OK, msg)
    }

    fun Route.getAll() = get {
        call.respond(countries)
    }

    fun Route.getByName() = get("/{name}") {
        val name = call.parameters["name"]
        if (name.isNullOrBlank()) {
            val msg = "Name cannot be null or blank."
            call.respond(HttpStatusCode.BadRequest, msg)
            return@get
        }

        val index = countries.indexOfFirst { it.name == name }
        if (index == -1) {
            val msg = "Country $name not found."
            call.respond(HttpStatusCode.NotFound, msg)
            return@get
        }

        call.respond(HttpStatusCode.OK, countries[index])
    }
}

fun Routing.installCountryRoute() {
    route("/country") {
        with(CountryRoute()) {
            authenticate {
                create()
                update()
                deleteByName()
                getAll()
                getByName()
            }

            //Kalau ada route yang tidak butuh authentication
            //install di luar block authenticate
        }
    }
}