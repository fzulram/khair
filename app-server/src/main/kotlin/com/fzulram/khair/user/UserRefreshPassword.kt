package com.fzulram.khair.user

/**
 * This is just for example.
 * If you're building a real app,
 * I suggest you to use a database to save this password.
 *
 * * key - name of the user
 * * value - refresh password
 *
 * If `(value == null)` = user is logged out
 */
val UserRefreshPassword = mutableMapOf<String, String?>()