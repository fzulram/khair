package com.fzulram.khair.jwt

import kotlinx.serialization.Serializable

/**
 * @author FzulRam.
 */
@Serializable
data class AuthToken(
    val accessToken: String,
    val refreshToken: String
)