package com.fzulram.khair.user

import kotlinx.serialization.Serializable

/**
 * @author FzulRam.
 */
@Serializable
data class User(
    val name: String,
    val password: String
)

/**
 * Contoh [User]
 */
val USER = User("FzulRam", "kunci")