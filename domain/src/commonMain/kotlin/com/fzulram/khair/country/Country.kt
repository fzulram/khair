package com.fzulram.khair.country

import kotlinx.serialization.Serializable

@Serializable
data class Country(
    val name: String,
    val population: Int
)

/**
 * Pakai global variable biar simple,
 * kalau di aplikasi beneran harusnya
 * pakai database.
 */
val countries = mutableListOf<Country>()