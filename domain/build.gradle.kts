plugins {
    kotlin("multiplatform")
    kotlin("plugin.serialization")
}

group = "com.fzulram"
version = "1.0-SNAPSHOT"

val serialization_version: String by project
fun serialization(name: String) =
    "org.jetbrains.kotlinx:" +
        "kotlinx-serialization-$name:" +
        serialization_version

kotlin {
    jvm {
        compilations.getting {
            kotlinOptions {
                jvmTarget = "11"
            }
        }
    }
    js(IR) {
        browser {
            testTask {
                useMocha()
            }

            binaries.executable()
        }
    }

    sourceSets {
        commonMain {
            dependencies {
                implementation(kotlin("stdlib-common"))
                implementation(serialization("json"))
            }
        }
        get("jvmMain").apply {
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
                implementation(serialization("json-jvm"))
            }
        }
        get("jvmTest").apply {
            dependencies {
                implementation(kotlin("test"))
            }
        }
        get("jsMain").apply {
            dependencies {
                implementation(kotlin("stdlib-js"))
                implementation(serialization("json-js"))
            }
        }
        get("jsTest").apply {
            dependencies {
                implementation(kotlin("test-js"))
            }
        }
    }
}