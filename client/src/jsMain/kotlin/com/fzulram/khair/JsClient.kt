package com.fzulram.khair

import io.ktor.client.*

actual class PlatformClientConfig {

    actual val installCommonConfig = true

    actual fun installTo(config: HttpClientConfig<*>) {
        config.apply {
            engine {
                //If you want to configure the engine
            }
        }
    }
}