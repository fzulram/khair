package com.fzulram.khair.country

import com.fzulram.khair.client
import io.ktor.client.request.*
import kotlinx.coroutines.runBlocking
import org.junit.Test
import kotlin.test.assertNotEquals
import kotlin.test.assertNotNull

/**
 * @author FzulRam.
 */
class CountryApiTest {

    @Test
    fun createTest(): Unit = runBlocking {
        val result = client.post<String> {
            url {
                encodedPath = "/country"
            }

//            body = Country("Indonesia", 1234)
        }

        assertNotNull(result)
        assertNotEquals("", result)
    }
}