package com.fzulram.khair.libs

import io.ktor.http.*

/**
 * @author FzulRam.
 */
data class EncodedPath(
    val path: String,
    val parameters: Parameters? = null
)