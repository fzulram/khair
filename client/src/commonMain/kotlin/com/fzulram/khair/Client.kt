package com.fzulram.khair

import com.fzulram.khair.jwt.AuthToken
import com.fzulram.khair.user.User
import io.ktor.client.*
import io.ktor.client.features.*
import io.ktor.client.features.auth.*
import io.ktor.client.features.auth.providers.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.features.logging.*
import io.ktor.client.request.*
import io.ktor.http.*

expect class PlatformClientConfig() {

    val installCommonConfig: Boolean

    fun installTo(config: HttpClientConfig<*>)
}

private val mClient get() = client
val client: HttpClient = HttpClient {
    val platformConfig = PlatformClientConfig()
    platformConfig.installTo(this)

    if (!platformConfig.installCommonConfig) {
        return@HttpClient
    }

    defaultRequest {
        host = "localhost"
        port = 8080
        contentType(ContentType.Application.Json)
    }

    install(Auth) {
        bearer {
            loadTokens {
                val response = mClient.post<AuthToken> {
                    url { encodedPath = "/user/login" }
                    body = User("FzulRam", "kunci")
                }

                BearerTokens(response.accessToken, "")
            }
            refreshTokens {
                val response = mClient.post<AuthToken> {
                    url { encodedPath = "/user/login" }
                    body = User("FzulRam", "kunci")
                }

                BearerTokens(response.accessToken, "")
            }
        }
    }

    //If I'm not wrong,
    //JsonFeature will be replaced by ContentNegotiation
    //in Ktor 2.0.0 (which now in beta version).
    //If the 2.0.0 version released,
    //I'll post about how the migration steps is, inshaallah.
    install(JsonFeature) {
        serializer = KotlinxSerializer()
    }

    install(Logging)
}