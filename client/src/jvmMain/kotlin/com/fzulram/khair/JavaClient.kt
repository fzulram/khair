package com.fzulram.khair

import io.ktor.client.*
import io.ktor.client.engine.java.*
import io.ktor.client.features.logging.*

actual class PlatformClientConfig {

    actual val installCommonConfig = true

    @Suppress("UNCHECKED_CAST")
    actual fun installTo(config: HttpClientConfig<*>) {
        (config as HttpClientConfig<JavaHttpConfig>).apply {
            engine {
                //If you want to configure the engine
            }

            install(Logging) {
                logger
            }
        }

    }
}