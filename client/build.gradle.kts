plugins {
    kotlin("multiplatform")
    kotlin("plugin.serialization")
}

group = "com.fzulram"
version = "1.0-SNAPSHOT"

val serialization_version: String by project
val coroutines_version: String by project
val ktor_version: String by project
val log4j_version: String by project
fun serialization(name: String) =
    "org.jetbrains.kotlinx:" +
        "kotlinx-serialization-$name:" +
        serialization_version

fun coroutines(
    name: String,
    version: String = coroutines_version
) = "org.jetbrains.kotlinx:" +
    "kotlinx-coroutines-$name:" +
    version

fun ktor(name: String) =
    "io.ktor:ktor-$name:$ktor_version"

fun log4J(name: String) =
    "org.apache.logging.log4j:$name:$log4j_version"

kotlin {
    jvm {
        compilations.getting {
            kotlinOptions {
                jvmTarget = "11"
            }
        }
    }
    js(IR) {
        browser {
            testTask {
                useMocha()
            }

            binaries.executable()
        }
    }

    sourceSets {
        commonMain {
            dependencies {
                implementation(kotlin("stdlib-common"))
                implementation(serialization("json"))
                implementation(coroutines("core-common", "1.3.8"))
                implementation(ktor("client-core"))
                implementation(ktor("client-auth"))
                implementation(ktor("client-logging"))
                implementation(ktor("client-serialization"))
                //In Ktor 2.0.0 : (remove client-serialization)
                //implementation(ktor("client-content-negotiation"))
                //implementation(ktor("serialization-kotlinx-json"))
                implementation(project(":domain"))
            }
        }
        get("jvmMain").apply {
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
                implementation(serialization("json-jvm"))
                implementation(coroutines("jdk8"))
                implementation(ktor("client-java"))
                implementation(log4J("log4j-slf4j-impl"))
            }
        }
        get("jvmTest").apply {
            dependencies {
                implementation(kotlin("test"))
            }
        }
        get("jsMain").apply {
            dependencies {
                implementation(kotlin("stdlib-js"))
                implementation(serialization("json-js"))
                implementation(coroutines("core-js"))
                implementation(ktor("client-js"))
            }
        }
        get("jsTest").apply {
            dependencies {
                implementation(kotlin("test-js"))
            }
        }
    }
}